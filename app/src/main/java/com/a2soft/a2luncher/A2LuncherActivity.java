package com.a2soft.a2luncher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

public class A2LuncherActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return NerdLuncherFragment.newInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }
}
